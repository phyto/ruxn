//#![warn(clippy::as_conversions)]

/// A uxn CPU. The flagship of the crate.
pub struct Cpu {
	mem: [u8; 0xffff],
	pc: u16,
	ws: Vec<u8>,
	rs: Vec<u8>,

	// internal
	k: bool,
	r: bool,
	short: bool,
}

#[derive(Clone, Copy, Debug)]
/// Returned after every CPU clock cycle. Describes anything relevant about that clock cycle.
pub enum Report {
	/// Nothing important happened.
	Normal,
	/// A BRK (0x00) was encountered, and should be handled by the wrapper.
	/// The wrapper can now go off and do whatever it wants (updating screen devices, listening for input...), and should only return to execution if it has a reason to.
	Break,
	/// DEO was called. Deo(port, value).
	Deo(u8, u8),
	/// DEO2 was called. Deo2(port, value).
	Deo2(u8, u16),
	/// DEI was called.
	Dei(u8),
}

/// A struct that contains information about the CPU.
pub struct CpuDump {
	pub mem: [u8; 0xffff],
	pub pc: u16,
	pub ws: Vec<u8>,
	pub rs: Vec<u8>,
}

use crate::uxn::Opcode;

impl Cpu {
	/// Take a uxn bytecode and build a CPU around it. Bytecode should start at 0x0100.
	pub fn from_uxn(mem: [u8; 0xffff]) -> Self {
		Self {
			mem,
			pc: 0x0100,
			ws: vec![],
			rs: vec![],

			k: false,
			r: false,
			short: false,
		}
	}
	/// Print some debug info to STDERR. Try not to use this if making a TUI (it WILL break your output).
	pub fn debug(&self) {
		eprintln!("pc: {:x}\nws: {:x?}\nrs: {:x?}", self.pc, self.ws, self.rs)
	}
	/// Clones data about the uxn cpu. May be useful for debugging.
	pub fn dump(&self) -> CpuDump {
		CpuDump {
			mem: self.mem,
			pc: self.pc,
			ws: self.ws.clone(),
			rs: self.rs.clone(),
		}
	}
	#[allow(dead_code)]
	/// Steps repeatedly, until the uxn does something interesting.
	///
	/// Stops if anything but a `Report::Normal` is returned.
	pub fn run(&mut self) -> Result<Report, Box<dyn std::error::Error>> {
		loop {
			let r = self.step();
			// self.debug();
			match r? {
				Report::Normal => continue,
				x => return Ok(x),
			}
		}
	}

	/// Executes exactly one instruction, also increments [`Self.pc`].
	pub fn step(&mut self) -> Result<Report, Box<dyn std::error::Error>> {
		let s = self;

		let opcode = {
			let (o, k, r, sh) = Opcode::from_byte(s.mem[s.pc as usize]);
			(s.k, s.r, s.short) = (k, r, sh);
			// dbg!(&o, k, r, sh);
			o
		};

		let (t, n, l) = (s.stack_get(0), s.stack_get(1), s.stack_get(2));

		s.pc += 1;

		match opcode {
			Opcode::BRK => return Ok(Report::Break),
			Opcode::JCI => {
				s.pop(1);
				if t? > 0 {
					let x = (((s.mem[s.pc as usize] as u16) << 8) + s.mem[s.pc as usize + 1] as u16)
						as i16 + 2;
					// dbg!(x);d
					s.pc = s.pc.wrapping_add_signed(x)
				} else {
					s.pc += 2;
				}
			}
			Opcode::JMI => {
				let x = raw_u8_to_i8(s.mem[s.pc as usize]);
				// dbg!(x);
				s.pc = s.pc.wrapping_add_signed(x as i16)
			}
			Opcode::JSI => {
				//TODO: do trolling instead
				let stack = match s.r {
					true => &mut s.ws,
					false => &mut s.rs,
				};
				let raddr = s.pc + 1;
				stack.push((raddr >> 8) as u8);
				stack.push((raddr & 0x00ff) as u8);

				let x = raw_u8_to_i8(s.mem[s.pc as usize]);
				// dbg!(x);
				s.pc = s.pc.wrapping_add_signed(x as i16)
			}
			Opcode::LIT => {
				if s.short {
					let mut x = s.mem[s.pc as usize] as u16;
					x <<= 8;
					x += s.mem[s.pc as usize + 1] as u16;
					s.push(x);
					s.pc += 2;
				} else {
					s.push(s.mem[s.pc as usize] as u16);
					s.pc += 1;
				}
			}

			Opcode::INC => {
				s.pop(1);
				s.push(t?.wrapping_add(1))
			}
			Opcode::POP => s.pop(1),
			Opcode::NIP => {
				s.pop(2);
				s.push(t?);
			}
			Opcode::SWP => {
				s.pop(2);
				s.push(t?);
				s.push(n?);
			}
			Opcode::ROT => {
				s.pop(3);
				s.push(n?);
				s.push(t?);
				s.push(l?);
			}
			Opcode::DUP => {
				s.pop(1);
				let t = t?;
				s.push(t);
				s.push(t)
			}
			Opcode::OVR => {
				s.pop(2);
				let n = n?;
				s.push(n);
				s.push(t?);
				s.push(n);
			}
			Opcode::EQU => {
				s.pop(2);
				s.bytepush((t? == n?) as u16)
			}
			Opcode::NEQ => {
				s.pop(2);
				s.bytepush((t? != n?) as u16)
			}
			Opcode::GTH => {
				s.pop(2);
				s.bytepush((n? > t?) as u16)
			}
			Opcode::LTH => {
				s.pop(2);
				s.bytepush((n? < t?) as u16)
			}

			Opcode::JMP => {
				s.pop(1);
				if s.short {
					s.pc = t?;
				} else {
					let x = raw_u8_to_i8(t? as u8);
					// dbg!(x);
					s.pc = s.pc.wrapping_add_signed(x as i16)
				}
			}
			Opcode::JCN => {
				if s.short {
					s.bytepop(3);
					let cond = n? & 0x00ff;
					if cond > 0 {
						s.pc = t?;
					}
				} else {
					s.pop(2);
					if n? > 0 {
						let x = raw_u8_to_i8(t? as u8);
						// dbg!(x);
						s.pc = s.pc.wrapping_add_signed(x as i16)
					}
				}
			}
			Opcode::JSR => {
				// do a little trolling
				s.r = !s.r;
				s.bytepush(s.pc >> 8);
				s.bytepush(s.pc & 0x00ff);
				s.r = !s.r;

				s.pop(1);

				if s.short {
					s.pc = t?;
				} else {
					let x = raw_u8_to_i8(t? as u8);
					dbg!(x);
					s.pc = s.pc.wrapping_add_signed(x as i16)
				}
			}

			Opcode::STH => {
				s.pop(1);
				s.r = !s.r;
				s.push(t?);
				s.r = !s.r;
			}
			Opcode::LDZ => {
				s.bytepop(1);
				let addr = (t? & 0x00ff) as usize;
				if s.short {
					let mut v = s.mem[addr] as u16;
					v <<= 8;
					v += s.mem[addr + 1] as u16;
					s.push(v);
				} else {
					s.push(s.mem[addr] as u16)
				}
			}
			Opcode::STZ => {
				let t = t?;
				let addr = (t & 0x00ff) as usize;
				if s.short {
					s.mem[addr] = s.bytestack_get(2)? as u8;
					s.mem[addr + 1] = ((t & 0xff00) >> 8) as u8;
					s.bytepop(3);
				} else {
					s.mem[addr] = n? as u8;
					s.bytepop(1);
				}
			}
			Opcode::LDR => {
				let addr = s
					.pc
					.wrapping_add_signed(raw_u8_to_i8((s.bytestack_get(0)? & 0x00ff) as u8) as i16)
					as usize;
				s.bytepop(1);
				if s.short {
					s.push(((s.mem[addr] as u16) << 8) + s.mem[addr + 1] as u16);
				} else {
					s.push(s.mem[addr] as u16);
				}
			}
			Opcode::STR => {
				let addr = s
					.pc
					.wrapping_add_signed(raw_u8_to_i8((s.bytestack_get(0)? & 0x00ff) as u8) as i16)
					as usize;
				if s.short {
					s.mem[addr] = s.bytestack_get(2)? as u8;
					s.mem[addr + 1] = s.bytestack_get(1)? as u8;
					s.bytepop(3)
				} else {
					s.mem[addr] = n? as u8;
				}
			}
			Opcode::LDA => {
				let addr = ((s.bytestack_get(1)? as usize) << 8) + s.bytestack_get(0)? as usize;
				s.bytepop(2);
				if s.short {
					s.push(((s.mem[addr] as u16) << 8) + s.mem[addr + 1] as u16);
				} else {
					s.push(s.mem[addr] as u16);
				}
			}
			Opcode::STA => {
				let addr = ((s.bytestack_get(1)? as usize) << 8) + s.bytestack_get(0)? as usize;
				if s.short {
					s.mem[addr] = s.bytestack_get(3)? as u8;
					s.mem[addr + 1] = s.bytestack_get(2)? as u8;
					s.bytepop(4);
				} else {
					s.mem[addr] = l? as u8;
					s.bytepop(3);
				}
			}

			Opcode::DEI => todo!(),
			Opcode::DEO => {
				let val = match s.short {
					true => (s.bytestack_get(2)? << 8) + s.bytestack_get(1)?,
					false => s.bytestack_get(1)?,
				};
				let port = s.bytestack_get(0)? as u8;
				s.pop(2 + s.short as usize);
				if s.short {
					return Ok(Report::Deo2(port, val));
				} else {
					return Ok(Report::Deo(port, val as u8));
				}
			}

			Opcode::ADD => {
				s.pop(2);
				s.push(t?.wrapping_add(n?));
			}
			Opcode::SUB => {
				s.pop(2);
				s.push(n?.wrapping_sub(t?));
			}
			Opcode::MUL => {
				s.pop(2);
				s.push(n?.wrapping_mul(t?));
			}
			Opcode::DIV => {
				s.pop(2);
				s.push(n? / t?);
			}
			Opcode::AND => {
				s.pop(2);
				s.push(n? & t?);
			}
			Opcode::ORA => {
				s.pop(2);
				s.push(n? | t?);
			}
			Opcode::EOR => {
				s.pop(2);
				s.push(n? ^ t?);
			}
			Opcode::SFT => {
				let right = s.bytestack_get(0)? & 0x0f;
				let left = (s.bytestack_get(0)? & 0xf0) >> 4;
				let val = if s.short {
					s.bytestack_get(1)? + (s.bytestack_get(2)? << 8)
				} else {
					s.stack_get(1)?
				};
				s.bytepop(1);
				s.pop(1);
				s.push((val >> right) << left);
			}
		}

		Ok(Report::Normal)
	}

	fn stack_get(&self, ofst: usize) -> Result<u16, String> {
		let s = match self.r {
			true => &self.rs,
			false => &self.ws,
		};

		if self.short {
			if ofst * 2 + 2 > s.len() {
				return Err("Stack empty".to_string());
			}
			let mut x: u16 = s[s.len() - 2 - ofst * 2] as u16;
			x <<= 8;
			x += s[s.len() - 1 - ofst * 2] as u16;
			Ok(x)
		} else {
			if ofst + 1 > s.len() {
				return Err("Stack empty".to_string());
			}
			Ok(s[s.len() - 1 - ofst] as u16)
		}
	}
	fn bytestack_get(&self, ofst: usize) -> Result<u16, String> {
		let s = match self.r {
			true => &self.rs,
			false => &self.ws,
		};

		if ofst + 1 > s.len() {
			return Err("Stack empty".to_string());
		}
		Ok(s[s.len() - 1 - ofst] as u16)
	}
	/// Pop the stack `n` times.
	fn pop(&mut self, mut n: usize) {
		if self.k {
			return;
		}

		let s = match self.r {
			true => &mut self.rs,
			false => &mut self.ws,
		};
		if self.short {
			n *= 2;
		}
		for _ in 0..n {
			if s.pop().is_none() {
				self.debug();
				eprintln!(" -- ERR: popped empty stack");
				//TODO: don't panic, it's rude
				panic!();
			}
		}
	}
	/// Pop a single byte, regardless of short mode.
	///
	/// Useful for jump opcodes which behave weirdly in short mode.
	fn bytepop(&mut self, n: usize) {
		if self.k {
			return;
		}

		let s = match self.r {
			true => &mut self.rs,
			false => &mut self.ws,
		};
		for _ in 0..n {
			s.pop().unwrap();
		}
	}
	fn push(&mut self, val: u16) {
		let s = match self.r {
			true => &mut self.rs,
			false => &mut self.ws,
		};
		if self.short {
			let v1 = val >> 8;
			let v2 = val & 0xff;
			s.push(v1 as u8);
			s.push(v2 as u8);
		} else {
			s.push(val as u8);
		}
	}
	// Like push, but ignore short mode
	fn bytepush(&mut self, val: u16) {
		let s = match self.r {
			true => &mut self.rs,
			false => &mut self.ws,
		};
		s.push(val as u8);
	}
}

/// Change the type without changing the bits
#[inline(always)]
fn raw_u8_to_i8(x: u8) -> i8 {
	let y: i8 = unsafe { std::mem::transmute(x) };
	assert_eq!(y, x as i8);

	y
}

/*

use std::fmt;
enum Err {
	StackEmpty,
}
impl fmt::Display for Err {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "An Error Occurred, Please Try Again!") // user-facing output
	}
}
impl fmt::Display for Err {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
	}
}
*/
