#![allow(dead_code)]

//! A uxn CPU.
//!
//! For varvara uxn, see [Ravara](https://git.disroot.org/phyto/ravara).
//!
//! This crate does not care which devices you want to use with it, and exposes an API to handle `DEI` and `DEO` events (among other things). For this reason it is suitable to build on top of.

pub mod cpu;
pub mod uxn;
