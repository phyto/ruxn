#![warn(missing_docs)]

//! Please don't use this crate as a binary. See the library documentation.

mod cpu;
mod uxn;
use cpu::Cpu;

fn main() {
	let mut mem = [0; 0xffff];
	{
		use std::io::prelude::*;
		let stdin = std::io::stdin();
		let mut total = 0;
		for (i, v) in stdin.bytes().enumerate() {
			total += 1;
			mem[i + 0x0100] = v.unwrap();
		}
		eprintln!("Read {total} bytes");
	}

	let mut x = Cpu::from_uxn(mem);
	loop {
		let r = x.step().unwrap();
		match r {
			cpu::Report::Break => break,
			_ => x.debug(),
		}
	}
}
