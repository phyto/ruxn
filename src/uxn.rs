use num_enum::TryFromPrimitive;

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Eq, PartialEq, TryFromPrimitive)]
#[repr(u8)]
/// Represents a single uxn opcode. Can be treated as a unit enum. in which the values match with the uxn spec.
#[allow(missing_docs)]
pub enum Opcode {
	BRK = 0,
	INC = 1,
	POP = 2,
	NIP = 3,
	SWP = 4,
	ROT = 5,
	DUP = 6,
	OVR = 7,
	EQU = 8,
	NEQ = 9,
	GTH = 10,
	LTH = 11,
	JMP = 12,
	JCN = 13,
	JSR = 14,
	STH = 15,
	LDZ = 16,
	STZ = 17,
	LDR = 18,
	STR = 19,
	LDA = 20,
	STA = 21,
	DEI = 22,
	DEO = 23,
	ADD = 24,
	SUB = 25,
	MUL = 26,
	DIV = 27,
	AND = 28,
	ORA = 29,
	EOR = 30,
	SFT = 31,
	// weird ones
	JCI = 0x20,
	JMI = 0x40,
	JSI = 0x60,
	LIT = 0x80,
}

impl Opcode {
	/* fn discriminant(&self) -> u8 {
		unsafe { *(self as *const Self as *const u8) }
	} */
	/// Takes in a uxn instruction in opcode form.
	/// Returns (Opcode, keep, return, short).
	pub fn from_byte(op: u8) -> (Self, bool, bool, bool) {
		// JCI, JMI, JSI
		if op == 0x20 || op == 0x40 || op == 0x60 {
			return (Opcode::try_from(op).unwrap(), false, false, false);
		}

		// modes
		let k = (op & 0x80) > 0;
		let r = op & 0x40 > 0;
		let short = op & 0x20 > 0;

		// LIT
		if op == 0x80 || op == 0xa0 || op == 0xc0 || op == 0xe0 {
			return (Opcode::LIT, false, r, short);
		}

		// everything else
		let opcode = Opcode::try_from(op % 0x20).unwrap();

		(opcode, k, r, short)
	}
}
